<?php
/**
 * Plugin Name: Super Product Tabs for WooCommerce Pro
 * Plugin URI: http://www.bemoore.com/
 * Description: Allows you to set custom tabs per product.
 * Version: 1.0.3
 * Author: Bob Moore, BeMoore Software
 * Author URI: http://www.bemoore.com/
 * License: GPL2
 */

//Save global settings
function wooc_super_product_tabs_pro_register_settings() {
	//register our settings
	register_setting( 'wooc-super-product-tabs-pro', 'new-tab-title' );
	register_setting( 'wooc-super-product-tabs-pro', 'new-tab-order' );
	register_setting( 'wooc-super-product-tabs-pro', 'meta_box_tabs' );
	register_setting( 'wooc-super-product-tabs-pro','meta-box-button-reset');
	
	$tabs = get_option('meta_box_tabs');
	
	foreach($tabs as $index => $values)
	{
		$setting_id = 'meta_box_tabs_'. $index; 
		register_setting( 'wooc-super-product-tabs-pro', $setting_id );	
	}	
}	

add_action('admin_init', 'wooc_super_product_tabs_pro_register_settings');


function wooc_super_product_tabs_pro_get_category_tabs(&$tabs,$term_id)
{
	$tabs_tmp = get_term_meta ($term_id, 'tabs');
	
	if(isset($tabs_tmp[0]))
	{
		$cat_tabs = $tabs_tmp[0];
		
		foreach($cat_tabs as $index => $value)
		{
			$tabs[$index] = $value;
			
			$tabs[$index]['hidden'] = '';
			if(!isset($cat_tabs[$index]['subcategories']))
				$tabs[$index]['subcategories'] = '';
			else
				$tabs[$index]['subcategories'] = 'checked';
		}
	}
}

function wooc_super_product_tabs_pro_get_global_tabs(&$tabs)
{
/*	
	$new_title = get_option('new-tab-title');
	$new_order = get_option('new-tab-order');
	$reset = get_option('meta-box-button-reset');

	$default_tabs = array();
	wooc_super_product_tabs_set_default_tabs($default_tabs);

	$tabs = get_option('meta_box_tabs');

	//Sync up the defaults with the saved ones.
	foreach($default_tabs as $default_id => $default_values)
	{
		if(!isset($tabs[$default_id]))
		{
			$tabs[$default_id] = $default_values;
			//unset($tabs[$default_id]['hidden']);	//Done for compatibility
		}
	}
		
	foreach($tabs as $id => $values)
	{
		if(isset($values['delete']))
		{
			unset($tabs[$id]);
			
			$setting_id = 'meta_box_tabs_'. $id; 
			delete_option($setting_id);
		}
		else
		{
			if(isset($values['hidden']))
				$tabs[$id]['hidden'] = 'checked';
			else
				unset($tabs[$id]['hidden']);
		}	
	}
	
	if($reset)
	{
		echo 'Resetting';
		$tabs = array();
		update_option('new-tab-title','');
		update_option('new-tab-order','');
		update_option('meta_box_tabs',array());
		update_option('meta-box-button-reset',false);
		return;
	}
	
	//Add any new tabs
	if($new_title != '')
	{
		$new_id = wooc_super_product_tabs_get_tab_index_from_title($new_title);
		$tabs[$new_id]['title'] = $new_title;
		$tabs[$new_id]['order'] = $new_order;
		$tabs[$new_id]['hidden'] = '';

		//Set the options
		update_option('new-tab-title','');
		update_option('new-tab-order','');
	}

	//print_r($tabs);
	
	update_option('meta_box_tabs',$tabs);

	//Now load the content (no need to save it again above)	
	foreach($tabs as $index => $values)
	{
		$setting_id = 'meta_box_tabs_'. $index; 
		$tabs[$index]['text'] = get_option($setting_id);
	}			*/
}
?>
