<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package BeMoore
 * @subpackage BeMoore
 * @since BeMoore 0.1
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'bemoore' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	<footer class="entry-meta">
		<?php edit_post_link( __( 'Edit', 'bemoore' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post -->
<?php 
/* Comment this in or out if you want comments on all pages. */
	if(of_get_option('display_page_comments'))
		comments_template( '', true );  
?>