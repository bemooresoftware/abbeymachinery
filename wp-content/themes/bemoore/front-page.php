<?php
/**
 * Template Name: Front Page Template
 *
 * Page template for Front Page
 *
 * @package BeMoore
 * @since BeMoore 0.1
 */

get_header(); ?>

	

<?php
	$imagepath =  get_template_directory_uri() . '/images/';
?>
<div class="row">
	<div class="col-md-12 top-front-page-widget">
		<?php if ( is_active_sidebar( 'top_front_page_widget' ) ) : ?>
		<?php dynamic_sidebar( 'top_front_page_widget' ); ?>	
		<?php endif; ?>	
	</div>
</div>
	<?php if(of_get_option('display_blurb') == '1'): ?>	
	<!--blurb-->
<div class="row">	
	<div class="col-md-12">
		<div class="jumbotron">	 
			<h1><?php echo of_get_option('blurb_heading'); ?></h1>
			<p class="lead"><?php echo of_get_option('blurb_text'); ?></p>
			<?php if(of_get_option('display_blurb_button') == '1'): ?>	
			<p><a class="btn btn-success btn-lg" href="<?php echo get_permalink( of_get_option('blurb_button_link_page')); ?>"><?php echo of_get_option('blurb_button_title'); ?></a></p>	 
			<?php endif; ?>	
		</div>
	</div>
</div>	
	<!--/blurb-->
	<?php endif; 
	$divcount = of_get_option('front_page_widget_section_count');

	$colwidth[1] = 'col-md-12';
	$colwidth[2] = 'col-md-4';
	$colwidth[3] = 'col-md-3';
	$colwidth[4] = 'col-md-3';
?>
	<div class="row">	
<?php	
	for($i=0;$i<$divcount;$i++)
	{
		$cols = $i+1;
			
		$widget_id = 'bemoore_front_page_'.$cols;
		?>		
		<div class="<?php echo $colwidth[$cols]; ?>" role="main" >
		<?php 
		if($i == 0)
			if ( has_nav_menu( 'side-menu' ) )
				wp_nav_menu( array( 'theme_location' => 'side-menu' ) ); 
		
		if ( is_active_sidebar( $widget_id ) ) : ?>
		<?php dynamic_sidebar( $widget_id ); ?>	
		<?php endif; ?>	
		</div>
		<?php
	}
	?>
	</div>
	<div class="row">
		<div class="col-md-12 bottom-front-page-widget">
			<?php if ( is_active_sidebar( 'bottom_front_page_widget' ) ) : ?>
			<?php dynamic_sidebar( 'bottom_front_page_widget' ); ?>	
			<?php endif; ?>	
		</div>
	</div>

<?php get_footer(); ?>
