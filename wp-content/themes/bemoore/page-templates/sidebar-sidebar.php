<?php
/**
 * Template Name: Template Sidebar Sidebar
 *
 * Page template for
 *
 * @package BeMoore
 * @since BeMoore 0.1
 */

get_header(); ?>



<div class="col-md-12">
<article>
	<header class="entry-header">
		<hgroup>

<h1 class="entry-title"><?php the_title(); ?></h1>
 </hgroup></header>


<?php get_sidebar('left6'); ?>
<?php get_sidebar('right6'); ?>
	
</article></div>
<?php get_footer(); ?>
