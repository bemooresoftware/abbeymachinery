<?php
/**
 * Template Name: Download Demo Template
 *
 * Page template for
 *
 * @package BeMoore
 * @since BeMoore 0.1
 */

get_header(); ?>


	<!-- Main Content -->	
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>			
					<?php get_template_part( 'content', 'page' ); ?>		
			<?php endwhile; ?>

		<?php else : ?>

			<h2><?php _e('No posts.', 'bemoore' ); ?></h2>
			<p class="lead"><?php _e('Sorry about this, I couldn\'t seem to find what you were looking for.', 'bemoore' ); ?></p>
			
		<?php endif; ?>		
		
		<?php bemoore_custom_pagination(); ?>
		</div>	
	</div>

	<!-- End Main Content -->	

	<div class="products downloads container">
		<?php
			$args = array(
				'post_type' => 'product',
				'posts_per_page' => 12,
				'product_cat' => 'downloads'  // category SLUG (if you want more than one just use commas)
				);
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
				while ( $loop->have_posts() ) : $loop->the_post();
					wc_get_template_part( 'content', 'product-download' );
				endwhile;
			} else {
				echo __( 'No products found' );
			}
			wp_reset_postdata();
		?>
	</ul><!--/.products-->	
	
	<?php			
	global $woocommerce;
	$cart_url = $woocommerce->cart->get_cart_url();
	echo '<a href="'.$cart_url.'">Get My Downloads</a>';
	?>	

<?php get_footer(); ?>

