<?php
/**
 * Template Name: Template Login
 *
 * Page template for
 *
 * @package BeMoore
 * @since BeMoore 0.1
 */

get_header(); ?>


<?php $col =  bemoore_get_content_cols(); ?>

	<!-- Main Content -->	
	<div class="col-md-<?php echo $col;?>" role="main">
	<?php wp_login_form(); ?>
	</div>	
	<!-- End Main Content -->	
<?php get_sidebar(); ?>	
<?php get_footer(); ?>

