<?php
/**
 * Template Name: Full Width Template
 *
 * Page template for
 *
 * @package BeMoore
 * @since BeMoore 0.1
 */

get_header(); ?>



	<!-- Main Content -->	
	
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>			
					<?php get_template_part( 'content', 'page' ); ?>		
			<?php endwhile; ?>

		<?php else : ?>

			<h2><?php _e('No posts.', 'bemoore' ); ?></h2>
			<p class="lead"><?php _e('Sorry about this, I couldn\'t seem to find what you were looking for.', 'bemoore' ); ?></p>
			
		<?php endif; ?>		
		
		<?php bemoore_custom_pagination(); ?>
		</div>	
	</div>
	<!-- End Main Content -->	

<?php get_footer(); ?>

