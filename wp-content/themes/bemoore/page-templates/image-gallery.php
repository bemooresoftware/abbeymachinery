<?php
/**
 * Template Name: Image Gallery Template
 *
 * Page template for
 *
 * @package BeMoore
 * @since BeMoore 0.1
 */
	$gallery_category_slug = "our-staff";	//The post category slug
	$nr_of_cols = 3;						//The # of cols
	$gallery_id = 'teachers';					//The gallery CSS ID	
	$include_captions = true;				//Include captions
	$include_descriptions = true;			//Include descriptions
	$image_size = 'thumbnail';				//The size of the thumbnail (hint : large is another one)

	get_header(); ?>
	
	

	<div class="row">
		<div class="col-md-12">
		<?php if ( is_active_sidebar( 'frontpage_slider_widget_area' ) ) 
				dynamic_sidebar( 'frontpage_slider_widget_area' );
		?>
		</div>	
	</div>	

	<!-- Main Content -->	
	<div class="col-md-12" role="main">
	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>			
				<?php get_template_part( 'content', 'page' ); ?>
				<!-- Gallery included here. You should be able to move this around -->
				<?php bemoore_gallery_listing($gallery_category_slug,$nr_of_cols,$gallery_id,$include_captions,$include_descriptions,$image_size); 	?>		
		<?php endwhile; ?>

	<?php else : ?>

		<h2><?php _e('No posts.', 'BeMoore' ); ?></h2>
		<p class="lead"><?php _e('Sorry about this, I couldn\'t seem to find what you were looking for.', 'BeMoore' ); ?></p>
		
	<?php endif; ?>		
</div>	
	<!-- End Main Content -->	



	
<?php get_footer(); ?>
