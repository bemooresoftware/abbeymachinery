<?php
/**
 * Template Name: Template Sidebar Content With Comments
 *
 * Page template for
 *
 * @package BeMoore
 * @since BeMoore 0.1
 */

get_header(); ?>


<?php $col =  bemoore_get_content_cols(); ?>
	
<?php get_sidebar('left'); ?>
	<!-- Main Content -->	
	<div class="col-md-<?php echo $col;?>" role="main">
	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>			
				<?php 
				/********************************************************************/
				/* This is what you need to change in your page template to get		*/
				/* comments. Page is content-pagewithcomments.php															*/
				/********************************************************************/
				get_template_part( 'content', 'pagewithcomments' ); ?>
		<?php endwhile; ?>
	<?php else : ?>
		<h2><?php _e('No posts.', 'bemoore' ); ?></h2>
		<p class="lead"><?php _e('Sorry about this, I couldn\'t seem to find what you were looking for.', 'bemoore' ); ?></p>		
	<?php endif; ?>			
	<?php bemoore_custom_pagination(); ?>
	</div>	
	<!-- End Main Content -->	
<?php get_footer(); ?>

