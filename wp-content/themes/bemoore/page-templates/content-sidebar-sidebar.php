<?php
/**
 * Template Name: Template Content Sidebar Sidebar
 *
 * Page template for
 *
 * @package BeMoore
 * @since BeMoore 0.1
 */

get_header(); ?>


	
	<!-- Main Content -->	
	<div class="col-md-6" role="main">
	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>			
				<?php get_template_part( 'content', 'page' ); ?>
		<?php endwhile; ?>
	<?php else : ?>
		<h2><?php _e('No posts.', 'bemoore' ); ?></h2>
		<p class="lead"><?php _e('Sorry about this, I couldn\'t seem to find what you were looking for.', 'bemoore' ); ?></p>		
	<?php endif; ?>			
	<?php bemoore_custom_pagination(); ?>
	</div>	
	<!-- End Main Content -->	
<?php get_sidebar('left'); ?>
<?php get_sidebar(); ?>



<?php get_footer(); ?>
