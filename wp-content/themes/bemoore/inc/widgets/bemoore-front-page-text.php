<?php
/**
 * Front Page Text
 *
 * File : bemoore-front-page-text.php
 * 
 * @package BeMoore
 * @since BeMoore 0.1
 */
?>
<?php

class bemoore_frontpage_text_widget extends WP_Widget {

         public function __construct() {

			/* Widget settings. */
			$widget_ops = array( 'classname' => 'front-page-text', 'description' => __('Front Paget Text for Marketing.', 'bemoore') );

			/* Widget control settings. */
			$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'widget-front-page-text' );

			/* Create the widget. */
			parent::__construct( 'widget-front-page-text', __('(BeMoore) Front Page Text', 'bemoore'), $widget_ops, $control_ops );		
		
        }
		
		/* Get Default values of fields. */
		function widget_defaults() {
			return array(
				'title' => '',
				'headline' => '',
				'tagline' => '',
				'image' => '',
				'action_url' => '',
				'action_label' => 'Learn More',
				'action_color' => 'primary',
				'alignment' => '',
				'thumbnail' => 'large',
			);
		}		

        public function form( $instance ) {
	
			$instance = wp_parse_args( (array) $instance, $this->widget_defaults());									  
				
			bemoore_widget_field( $this, array ( 'field' => 'title', 'label' => __( 'Title:', 'bemoore' ) ), $instance['title'] );
			bemoore_widget_field( $this, array ( 'field' => 'image', 'label' => __( 'Image:', 'bemoore' ), 'type' => 'media' ), $instance['image'] );
			bemoore_widget_field( $this, array ( 'field' => 'thumbnail', 'type' => 'select', 
												   'label' => __( 'Image Size:', 'bemoore' ), 
												   'options' => bemoore_thumbnail_array(), 
												   'class' => '' ), $instance['thumbnail'] );
			if ( $instance['image'] )
				echo wp_get_attachment_image( $instance['image'], bemoore_thumbnail_size( $instance['thumbnail'] ), false, array( 'class' => 'widget-image' ) );
				
			bemoore_widget_field( $this, array ( 'field' => 'headline', 'label' => __( 'Headline:', 'bemoore' ) ), $instance['headline'] );
			bemoore_widget_field( $this, array ( 'field' => 'tagline', 'label' => __( 'Tagline:', 'bemoore' ), 'type' => 'textarea' ), $instance['tagline'] );
			bemoore_widget_field( $this, array ( 'field' => 'action_url', 'label' => __( 'Action URL:', 'bemoore' ), 'type' => 'url' )
									, $instance['action_url'] );
									
			bemoore_widget_field( $this, array ( 'field' => 'action_label', 'label' => __( 'Action Label:', 'bemoore' ) ), $instance['action_label'] );
			
			bemoore_widget_field( $this, array ( 'field' => 'alignment', 'type' => 'select', 
												   'label' => __( 'Alignment: ', 'bemoore' ),
												   'options' => array (
														array(	'key' => 'left',
																'name' => __( 'Left', 'bemoore' ) ),
														array(	'key' => 'center',
																'name' => __( 'Center', 'bemoore' ) ),
														array(	'key' => 'right',
																'name' => __( 'Right', 'bemoore' ) ),			
																 ), 'class' => '' ), $instance['alignment'] );
																 
			bemoore_widget_field( $this, array ( 'field' => 'action_color', 'type' => 'select', 
															   'label' => __( 'Action Button: ', 'bemoore' ),
															   'options' => array (
																	array(	'key' => 'primary',
																			'name' => __( 'Primary', 'bemoore' ) ),
																	array(	'key' => 'info',
																			'name' => __( 'Info', 'bemoore' ) ),
																	array(	'key' => 'warning',
																			'name' => __( 'Warning', 'bemoore' ) ),
																	array(	'key' => 'danger',
																			'name' => __( 'Danger', 'bemoore' ) ),
																	array(	'key' => 'success',
																			'name' => __( 'Success', 'bemoore' ) ),													
																	array(	'key' => 'default',
																			'name' => __( 'Default', 'bemoore' ) ),				
																			 ), 'class' => '' ), $instance['action_color'] );																 
			   
        }

        public function update( $new, $old ) {				
			$instance = $old;
			$instance['title'] = strip_tags( $new['title'] );
			$instance['headline'] = wp_kses_stripslashes($new['headline']);
			$instance['tagline'] = wp_kses_stripslashes($new['tagline']);
			$instance['image'] =  $new['image'];
			$instance['thumbnail'] = $new['thumbnail'];
			$instance['action_url'] = esc_url_raw($new['action_url']);
			$instance['action_label'] = wp_kses_stripslashes($new['action_label']);
			$instance['action_color'] = wp_kses_stripslashes( $new['action_color'] );
			$instance['alignment'] = wp_kses_stripslashes( $new['alignment'] );			
			return $instance;			
        }

        public function widget( $args, $instance ) {	
		
			extract( $args, EXTR_SKIP );
			$instance = wp_parse_args($instance, $this->widget_defaults());
			extract( $instance, EXTR_SKIP );
			$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base);		

			echo $before_widget; 
			if ( ! empty( $title ) ) {
				echo $before_title;
				echo $title;
				echo $after_title;
			} 
			echo '<div class="frontpage-widget-inner-text text-'.esc_attr( $alignment ).'">';
			if ( ! empty( $image ) ) {
				if ( ! empty( $action_url ) )
					echo '<a href="' . esc_url( $action_url ) . '">';
				echo wp_get_attachment_image( $image, bemoore_thumbnail_size( $thumbnail ) );
				if ( ! empty( $action_url ) )
					echo '</a>';			
			}
			
			if ( ! empty( $headline ) )
				echo '<h2>' . esc_attr( $headline ) . '</h2>';
			if ( ! empty( $tagline ) )
				echo '<p>' . do_shortcode( $tagline ) .'</p>';
			if ( ! empty( $action_url ) && ! empty( $action_label ) ) {
				echo '<p><a href="' . esc_url( $action_url );
				echo '" class="action-label btn btn-' . esc_attr( $action_color ) . '">';
				echo esc_attr( $action_label ) . '</a></p>';
			}
			echo '</div>';

			echo $after_widget;

        }
}

?>
