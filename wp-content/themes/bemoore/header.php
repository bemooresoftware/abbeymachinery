<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package BeMoore
 * @subpackage BeMoore
 * @since BeMoore 0.1
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>


<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="print" href="<?php bloginfo('stylesheet_directory'); ?>/print.css" />
<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo of_get_option('google_analytics_code'); ?>', 'auto');
  ga('send', 'pageview');
</script>
</head>

<body <?php body_class(); ?>>
	<div id="bodychild">
	<!-- Wrap all page content here -->  
	<div id="wrap">	
	
	<?php 
		$site_logo = of_get_option('site_logo');
		
		$display_nav_search = of_get_option('display_nav_search');
	?>	
		<header class="site-header" role="banner">
		<div id="header-top">
			<div class="container hidden-xs">
				<div class="pull-right"  id="header-top-container">
					<div class="pull-right">
						<div class="pull-left">
						<?php 
							wp_nav_menu( array( 'theme_location' => 'secondary', 
												'menu_class' => 'list-inline', 
												'depth' =>1, 
												'container' => false, 
												'fallback_cb' => false ) ); 
						?>
						</div>
						<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) :?>
						<div class="woocommerce-header-cart pull-right">						
							<?php global $woocommerce; ?>
							<a href="<?php echo $woocommerce->cart->get_cart_url(); ?>">
								<?php if ($woocommerce->cart->cart_contents_count == 0){
										printf( '<i class="icon-shopping-cart"></i>', get_stylesheet_directory_uri());
									}else{
										printf( '<i class="icon-shopping-cart"></i>', get_stylesheet_directory_uri());
									}
								?>  
							</a>
							<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" >
							Your Cart : <?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
						</div>
						<?php endif;?>
						
					</div>		
				</div>
			</div>			
		</div>	

		<div class="header-body">		
			<div class="container">
				 <div class="row logo-row">
				  <div class="col-md-4 pull-left">
					<?php if ( $site_logo != '' ) : ?>
					<a href="<?php echo esc_url( home_url( '/' )); ?>"><img src="<?php echo esc_url($site_logo); ?>" alt="<?php bloginfo('description'); ?>" class="img-responsive" /></a>
					<?php elseif($site_logo == '' || !isset($site_logo)): ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<small><?php bloginfo( 'description' ); ?></small>
					<?php endif; ?>					
				  </div>	  
				  <div class="col-md-8 hidden-xs">
					<div class="pull-right">
						<?php if ( is_active_sidebar( 'bemoore_header_right' ) ) : ?>
							<?php dynamic_sidebar( 'bemoore_header_right' ); ?>	
						<?php endif; ?>	
					</div>
				  </div>
				</div>
			</div>	
		</div>
	
	</header>

    <!-- Fixed navbar -->
	
	<?php if (function_exists ( 'ubermenu' ) ): ?>
		<div class="new-menu col-md-12" style="padding-left:0px; padding-right:0px;">
		<?php ubermenu( 'main' , array( 'theme_location' => 'primary' ) ); ?>
		</div>
	<?php else :?>	
			<div class="navbar navbar-inverse navbar-static-top" role="navigation">
			  <div class="container">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <!--
				  <a class="navbar-brand visible-xs" href="<?php echo esc_url (home_url( '/' )); ?>"><i class="icon-home"></i></a>
				  -->
				<?php if(isset($display_nav_search) && $display_nav_search==true): ?> 
				<div class="navbar-search-sm pull-right visible-sm visible-xs">

						<form class="navbar-search navbar-form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<input type="search" name="s" id="s" class="search-fields" placeholder="<?php esc_attr_e( 'Search', 'bemoore' ); ?>" name="s">
						</form>

				</div>		
				<?php endif; ?>
				
				</div>
				<div class="navbar-collapse collapse">
				<?php wp_nav_menu( array( 
									'theme_location' => 'primary', 
									'menu_class' => 'nav navbar-nav nav-justified', 
									'depth' =>4,
									'container' => false, 
									'fallback_cb' => false, 
									'walker' => new bemoore_theme_navigation() ) ); ?>	
				
				<?php if(isset($display_nav_search) && $display_nav_search==true): ?> 	
				<ul class="nav navbar-nav navbar-right visible-md visible-lg pull-right">
					<li>
						<form class="navbar-search navbar-form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<input type="search" name="s" id="s" class="search-fields" placeholder="<?php esc_attr_e( 'Search', 'bemoore' ); ?>" name="s">
						</form>
					</li>
				</ul>
				<?php endif; ?>		
									
				</div><!--/.nav-collapse -->
			  </div>
			</div> <!-- / fixed navbar--> 
	
	<?php endif; ?>
	
	<!--slider widget -->
	<?php if(is_front_page()) {?>
	<div class="col-md-12">
	<div class="row slider">
		<?php if ( is_active_sidebar( 'slider' ) ) : ?>
		<?php dynamic_sidebar( 'slider' ); ?>	
		<?php endif; ?>	
	</div>
	</div>
	<?php } ?>	
	<!--slider-widget-->
<?php
	$class = 'row';
	
	if(is_front_page())
		$class .= ' frontpage';
?>

    <div class="<?php echo $class;?>" id="main-container">
	<div class="row" id="main-row">

