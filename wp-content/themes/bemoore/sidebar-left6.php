<?php
/**
 * Sidebar Left 6
 *
 * Content for our sidebar, provides prompt for logged in users to create widgets
 *
 * @package BeMoore
 * @subpackage BeMoore
 * @since BeMoore 0.1
 */
?>

<!-- Sidebar -->
<div class="col-md-6 sidebar-left" >

<?php if ( dynamic_sidebar('bemoore_sidebar_left') ) : elseif( current_user_can( 'edit_theme_options' ) ) : ?>

	<h5><?php _e( 'No widgets found.', 'bemoore' ); ?></h5>
	<p><?php printf( __( 'It seems you don\'t have any widgets in your sidebar! Would you like to %s now?', 'bemoore' ), '<a href=" '. get_admin_url( '', 'widgets.php' ) .' ">populate your sidebar</a>' ); ?></p>

<?php endif; ?>

</div>
<!-- End Sidebar -->