<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package BeMoore
 * @subpackage BeMoore
 * @since BeMoore 0.1
 */
?>
</div> <!-- #bodychild close -->
</div> <!-- #main-row-->
</div> <!-- .container #main-container-->
</div> <!-- #wrap -->

<footer>
		<?php
		/* A sidebar in the footer? Yep. You can customize
		 * your footer with three columns of widgets.
		 */
			get_sidebar( 'footer' );		
			$copyrighttxt = of_get_option('copyright_text');
			$copyrighttxt = ! empty($copyrighttxt) ? $copyrighttxt : __('&copy; ', 'bemoore') . __(date('Y')) . sprintf(' <a href="%1$s" title="%2$s" rel="home">%3$s</a>', esc_url(home_url( '/' )), get_bloginfo( 'name' ), get_bloginfo( 'name' ));
			$d=sprintf('<a href="%1$s" title="%2$s" rel="home">%3$s</a>', esc_url(home_url( '/' )), get_bloginfo( 'name' ), get_bloginfo( 'name' ));			
		?>
			<div class="carousel col-md-12">
					<?php if ( is_active_sidebar( 'carousel' ) ) : ?>
					<?php dynamic_sidebar( 'carousel' ); ?>	
					<?php endif; ?>	
			</div>		
					
  <div class="container-fluid footer-nav" id="footer" style="padding-top:0px;">	
	
	<?php 
		wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'list-inline', 'depth' =>1, 'container' => false, 'fallback_cb' => false ) ); 
	?>
	
	</div>
	
	<div class="container">	
		<div class="col-md-12 address"> 
			<?php if ( is_active_sidebar( 'address' ) ) : ?>
			<?php dynamic_sidebar( 'address' ); ?>	
			<?php endif; ?>	
		</div>

		<div class="col-md-12 powered ">
			<div class="pull-right hidden-xs">
				<p class="text-muted credit">Powered by <a href="http://www.bemoore.com" target="blank"> Bemoore Software Consultants </a></p>
			</div> 	
		</div>
	</div>
</footer> 
</div> <!-- #bodychild -->
<?php wp_footer(); ?>
</body>
</html>
