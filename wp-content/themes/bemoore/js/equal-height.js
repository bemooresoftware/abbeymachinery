jQuery.noConflict();

jQuery( document ).ready(function( $ ) {

	//Do it on load
	scale_sidebar();
	
	//Also do it when window is resized.
	$( window ).resize(function() {
		scale_sidebar();
	});
	
	function scale_sidebar()
	{
		var offset = 50; // no idea why this is needed 
		
		var collapse_point = 1024; // The point at which the menu collapses 
		var width = window.innerWidth;

		var sidebar =  $('.sidebar-right');	
		var main = $('div[role=main]');
		
		var sidebar_height = sidebar.outerHeight(true);
		var main_height = main.outerHeight(true);

		if(main_height == null)
		{
			main = $('div[role=content]');
			main_height = main.outerHeight(true);
		}
		
		if(sidebar_height == null)
		{
			sidebar = $('.sidebar-left');
			sidebar_height = sidebar.outerHeight(true);
		}
		
		
		//We don't want little squashed pages ...
		//if(sidebar_height < 500 && main_height < 500)
		//	return;
	
		// We only need this to happen when the menu is not collapsed 
		if(width < collapse_point)
		{
			main.css('height', '');
			sidebar.css('height', '');
		}
		else if(sidebar_height < 500 && main_height < 500)
		{
			main.css('height', '');
			sidebar.css('height', '');
		}
		else
		{
			if(sidebar_height > main_height)
				main.outerHeight(sidebar_height);
			else
				sidebar.outerHeight(main_height + offset);		
		}
	}
});

/*
jQuery.noConflict();

jQuery('#main-row div').each(function() {
	
	//alert('called');
  var eHeight = jQuery(this).innerHeight();

  jQuery(this).find('div').outerHeight(eHeight);
});
*/