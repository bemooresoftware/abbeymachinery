<?php
/**
 * Template Name: Front Page Template
 *
 * Page template for Front Page
 *
 * @package BeMoore
 * @since BeMoore 0.1
 */

get_header(); ?>

	

<?php
	$imagepath =  get_template_directory_uri() . '/images/';
?>
<div class="row outside-row">
<div class="row">
	<div class="col-md-12 top-front-page-widget">
		<?php if ( is_active_sidebar( 'top_front_page_widget' ) ) : ?>
		<?php dynamic_sidebar( 'top_front_page_widget' ); ?>	
		<?php endif; ?>	
	</div>
</div>
	<?php if(of_get_option('display_blurb') == '1'): ?>	
	<!--blurb-->
<div class="row">	
	<div class="col-md-12">
		<div class="jumbotron">	 
			<h1><?php echo of_get_option('blurb_heading'); ?></h1>
			<p class="lead"><?php echo of_get_option('blurb_text'); ?></p>
			<?php if(of_get_option('display_blurb_button') == '1'): ?>	
			<p><a class="btn btn-success btn-lg" href="<?php echo get_permalink( of_get_option('blurb_button_link_page')); ?>"><?php echo of_get_option('blurb_button_title'); ?></a></p>	 
			<?php endif; ?>	
		</div>
	</div>
</div>	
	<!--/blurb-->
	<?php endif; 
	$divcount = of_get_option('front_page_widget_section_count');

	$colwidth[1] = 'col-md-7';
	$colwidth[2] = 'col-md-5';
	$colwidth[3] = 'col-md-3';
	$colwidth[4] = 'col-md-3';
?>
	<div class="row">	
<?php	
	for($i=0;$i<$divcount;$i++)
	{
		$cols = $i+1;
			
		$widget_id = 'bemoore_front_page_'.$cols;
		?>		
		<div class="<?php echo $colwidth[$cols]; ?>" role="main" >
		<?php 
		if($i == 0)
			if ( has_nav_menu( 'side-menu' ) )
				wp_nav_menu( array( 'theme_location' => 'side-menu' ) ); 
		
		if ( is_active_sidebar( $widget_id ) ) : ?>
		<?php dynamic_sidebar( $widget_id ); ?>	
		<?php endif; ?>	
		</div>
		<?php
	}
	?>
	</div>
	<div class="row">
		<div class="row col-md-12 second-row">
			<div class="col-md-7">
				<?php if ( is_active_sidebar( 'production' ) ) : ?>
			<?php dynamic_sidebar( 'production' ); ?>	
			<?php endif; ?>	
			<?php if ( is_active_sidebar( 'qparts' ) ) : ?>
			<?php dynamic_sidebar( 'qparts' ); ?>	
			<?php endif; ?>	


			</div>
			<div class="col-md-5">

			<?php if ( is_active_sidebar( 'about' ) ) : ?>
			<?php dynamic_sidebar( 'about' ); ?>	
			<?php endif; ?>	


			<?php if ( is_active_sidebar( 'sale' ) ) : ?>
			<?php dynamic_sidebar( 'sale' ); ?>	
			<?php endif; ?>	
			
			<?php if ( is_active_sidebar( 'testimonials' ) ) : ?>
			<?php dynamic_sidebar( 'testimonials' ); ?>	
			<?php endif; ?>	
			</div>
		</div>
	</div>
	
	
	<div class="row">
		<div class="row col-md-12 bottom-front-page-widget">
		
		<h1 style="text-align:center"> Connect with Us </h1>
		<div class="col-md-4 news">
			<?php if ( is_active_sidebar( 'news' ) ) : ?>
			<?php dynamic_sidebar( 'news' ); ?>	
			<?php endif; ?>	
			

			
			
		</div>
		<div class="col-md-4 facebook">
			<?php if ( is_active_sidebar( 'facebook' ) ) : ?>
			<?php dynamic_sidebar( 'facebook' ); ?>	
			<?php endif; ?>	
		</div>
		<div class="col-md-4 twitter">
			<?php if ( is_active_sidebar( 'twitter' ) ) : ?>
			<?php dynamic_sidebar( 'twitter' ); ?>	
			<?php endif; ?>	
		</div>
	
	</div>
	</div>
	

<?php get_footer(); ?>
