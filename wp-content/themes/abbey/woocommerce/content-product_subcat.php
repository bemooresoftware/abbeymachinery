<?php
	$taxonomy_name = 'product_cat';
	$children = get_term_children( $category->term_id, $taxonomy_name );

	foreach ( $children as $child ) { ?>
		<div class="col-md-6 subcat-main" >
			<div class="subcat-wrapper">
			<?php		
				$term = get_term_by( 'id', $child, $taxonomy_name );
				$term_url = get_term_link( $child, $taxonomy_name );
				
				$thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
				$image = wp_get_attachment_url( $thumbnail_id ,'thumbnail');
?>				
				<div class="col-md-12 subcat-title"><h4><a href="<?php echo $term_url;?>"><?php echo $term->name; ?></a></h4></div>
<?php				
				if ( $image ) { ?>
					<div class="col-md-12 subcat-image">
					<a href="<?php echo $term_url;?>"><img src="<?php echo $image; ?>" alt="" /></a>
					</div>
				<?php }	?>
				<div class="col-md-12 subcat-readmore cart-div pull-left"><a rel="nofollow" href="<?php echo $term_url;?>" class="button product_type_simple">Read More</a></div>
			</div>
		</div>
<?php } ?>
