<?php
/**
 * The template for displaying product category thumbnails within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product_cat.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $woocommerce_loop;

//echo "called : loop is : ".$woocommerce_loop['loop'];

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
	$col_div_count = 0;
	
	//There is no sidebar ....
/*	if(!bemoore_has_shop_category_sidebar())
	{*/
		$col_div_count = 12 / (int)$woocommerce_loop['columns'];
		//echo "No Sidebar loop: ".$woocommerce_loop['columns']." cols : $col_div_count"; 		
/*	}
	else //There is a sidebar so find out how wide it is ...
	{
		$col_div_count = floor((12  - bemoore_get_sidebar_cols()) / (int)$woocommerce_loop['columns']);	
		
		echo "Sidebar loop: ".$woocommerce_loop['columns']." cols : $col_div_count"; 
	}
*/	
// Increase loop count
//$woocommerce_loop['loop']++;
?>
<div class="col-md-<?php echo $col_div_count; ?> col-sm-<?php echo $col_div_count; ?> product-category product<?php
    if ( ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] == 0 || $woocommerce_loop['columns'] == 1 )
        echo ' first';
	if ( $woocommerce_loop['loop'] % $woocommerce_loop['columns'] == 0 )
		echo ' last';
	?>">

	<?php do_action( 'woocommerce_before_subcategory', $category ); ?>

	<a href="<?php echo get_term_link( $category->slug, 'product_cat' ); ?>">

		<?php
			/**
			 * woocommerce_before_subcategory_title hook
			 *
			 * @hooked woocommerce_subcategory_thumbnail - 10
			 */
			do_action( 'woocommerce_before_subcategory_title', $category );
		?>

		<h3 class="product_cat">
			<?php
				echo $category->name;

				if ( $category->count > 0 )
					echo apply_filters( 'woocommerce_subcategory_count_html', ' <mark class="count">(' . $category->count . ')</mark>', $category );
			?>
		</h3>
		<?php if($category->description != "")
				echo '<p class="category_description">'.$category->description.'</p>'; 

			/**
			 * woocommerce_after_subcategory_title hook
			 */
			do_action( 'woocommerce_after_subcategory_title', $category );
		?>

	</a>

	<?php do_action( 'woocommerce_after_subcategory', $category ); 	?>
</div>
