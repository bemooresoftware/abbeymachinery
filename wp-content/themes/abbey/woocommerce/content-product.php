<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.1
 */


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 2 );

	//echo "shop product cols : ".$woocommerce_loop['columns'];

$col_div_count = 0;
$col_div_count = 12 / (int)$woocommerce_loop['columns'];

// Extra post classes
$classes = array();

if($woocommerce_loop['loop'] == 0)
	echo '<div class="row" >';
else if((int)($woocommerce_loop['loop']) % (int)$woocommerce_loop['columns'] == 0)
{
	echo '</div>';
	echo '<div class="row" >';
}
$classes[] = 'col-md-'.$col_div_count;
$classes[] = 'product';
?>

<div <?php post_class( $classes ); ?>   >


	<?php  do_action( 'woocommerce_before_shop_loop_item' );   ?>

	
	<h4 class="product-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	
	
	
	<div class="product-picture">
		
		<a href="<?php the_permalink(); ?>" class="product-thumbnail" style="height:250px; width:auto;"><?php the_post_thumbnail(); ?></a>
	</div>
	
	<div class="price col-md-12 pull-left">
		<?php if ( $price_html = $product->get_price_html() ) : ?>
		<?php echo $price_html; ?>
		<?php endif; ?>
	</div>
	<div class="stock col-md-12 pull-left">
<?php	
    if ( $product->is_in_stock() )
        echo '<div class="stock" >' . $product->get_stock_quantity() . __( ' in stock', 'bemoore' ) . '</div>';
    else
        echo '<div class="out-of-stock" >' . __( 'out of stock', 'bemoore' ) . '</div>';
?>	
	</div>
		<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			/* do_action( 'woocommerce_before_shop_loop_item_title' ); */
		?>

		

		<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			/* do_action( 'woocommerce_after_shop_loop_item_title' ); */
		?>

	
	<div class="cart-div col-md-12 ">
		<?php do_action( 'woocommerce_after_shop_loop_item' ); // Cart button here ?>	
	</div> 
</div>
