<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package BeMoore
 * @subpackage BeMoore
 * @since BeMoore 0.1
 */
?>
<?php 
	$display_post_meta_info = of_get_option('display_post_meta_info');
	$show_blog_comments = of_get_option('display_blog_comments');
?>
	<p>CLICK ON THE ICONS BELOW TO VIEW/DOWNLOAD THE RELEVANT BROCHURE REQUIRED.</p>
	<p>A PDF viewer such as Adobe or PDF Complete is required to view the brochure.</p>
<?php
	$pdf = get_field('download_pdf');
	$pdf_image = get_field('pdf_image');
?>
	<div class="downloads-wrap" >
		<div class="row col-md-12 download">
				<div class="col-md-12 download-title" >
					<h2><a target="_blank" href="<?php echo $pdf; ?>"><?php the_title() ?></a></h2>
				</div>
				<div class="col-md-6 download-link">
					<a target="_blank" href="<?php echo $pdf; ?>"><img class="pdf-image" src="<?php echo $pdf_image['url']; ?>" alt="pdf logo" /></a>
				</div>
				<div class="row col-md-6">
					<div class="col-md-12 download-description" >
						<?php the_content(); ?>
					</div>
					<div class="col-md-6 download-button">
						<a target="_blank" href="<?php echo $pdf; ?>">Download</a>
					</div>
				</div>
		</div>
	</div>
