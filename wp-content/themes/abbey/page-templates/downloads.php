<?php
/**
 * Template Name: downloads
 *
 * Page template for
 *
 * @package BeMoore
 * @since BeMoore 0.1
 */

get_header(); ?>

<p>CLICK ON THE ICONS BELOW TO VIEW/DOWNLOAD THE RELEVANT BROCHURE REQUIRED.</p>
	<p>A PDF viewer such as Adobe or PDF Complete is required to view the brochure.</p>

<?php $col =  bemoore_get_content_cols(); ?>

	<!-- Main Content -->	
	<div class="col-md-9" role="main">
	
	<?php 
	//Add Pagination Start
	global $paged,$wp_query;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	//Add Pagination End : Also note $paged argument in $args, and also the query has to be called $wp_query	
	$args = array( 'post_status' => 'publish' , 'post_type' => array( 'downloads' ),'paged' => $paged  );
	
	$wp_query = new WP_Query( $args );
	
	global $post;
	
	while ( $wp_query->have_posts() ) :
		$wp_query->the_post();
		/* Put your post record here */
		$pdf = get_field('download_pdf');
		$pdf_image = get_field('pdf_image');
	?>
		<div class="downloads-wrap" >
			<div class="row col-md-12 download">
					<div class="col-md-12 download-title" >
						<h2><a target="_blank" href="<?php echo $pdf; ?>"><?php the_title() ?></a></h2>
					</div>
					<div class="col-md-6 download-link">
						<a target="_blank" href="<?php echo $pdf; ?>"><img class="pdf-image" src="<?php echo $pdf_image['url']; ?>" alt="pdf logo" /></a>
					</div>
					<div class="row col-md-6">
						<div class="col-md-12 download-description" >
							<?php the_content(); ?>
						</div>
						<div class="col-md-6 download-button">
							<a target="_blank" href="<?php echo $pdf; ?>">Download</a>
						</div>
					</div>
			</div>
		</div>
		
	<?php				/* ends */
	endwhile;
	
	wp_reset_postdata();
?>
	</div>	

	<!-- End Main Content -->	
<?php get_sidebar(); ?>	


<?php get_footer(); ?>

