jQuery(document).ready(function($) {
	
	$("div.new-menu").sticky({topSpacing:0,zIndex:10000});
	
	static();
   
    $("div.new-menu").on('sticky-start', function() { floating(); });
	$("div.new-menu").on('sticky-end', function() { static(); });
	
	function floating()
	{
		$('img.ubermenu-image').css('width: 100%');
		$('img.ubermenu-image').show();
		$('li#menu-item-225 > a > span').hide();		
	}
	
	function static()
	{
		$('.ubermenu-main .ubermenu-item-layout-image_left > .ubermenu-target-text').css('padding-left: 0px');
		$('img.ubermenu-image').hide();
		$('li#menu-item-225 > a > span').show();		
	}
})
