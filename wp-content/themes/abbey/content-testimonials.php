<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package BeMoore
 * @subpackage BeMoore
 * @since BeMoore 0.1
 */
?>

<?php 
	$display_post_meta_info = of_get_option('display_post_meta_info');
	$show_blog_comments = of_get_option('display_blog_comments');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
	<header>
		<hgroup>
			<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'bemoore' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
		
		<hr class="post-meta-hr"/>
		</hgroup>
	</header>

	<?php if ( has_post_thumbnail()) : ?>
		<div class="featured-img pull-left">
		<?php the_post_thumbnail('thumbnail'); ?>
		</div>
	<?php endif; ?>
	
	<div class="entry-summary">
	<?php the_content(); ?>
	<?php 
	if($show_blog_comments)
		comments_template( '', true ); 
	?>		
	</div><!-- .entry-summary -->
	<div class="clearfix"/>
</article>

<!--<hr>-->
	  
	 