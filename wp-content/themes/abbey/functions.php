<?php

								// Sidebar bemoore_header_left
	register_sidebar( array(
			'id' => 'bemoore_header_left',
			'name' => __( 'bemoore_header_left' ),
			'description' => __( 'This widget is positioned in the first row of the homepage' ),
			'before_widget' => '<div id="%1$s" class="bemoore_header_left %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="bemoore_header_left-title">',
			'after_title' => '</h4>',
		) );

		
		// text under logo
	register_sidebar( array(
			'id' => 'text',
			'name' => __( 'text' ),
			'description' => __( 'This widget is positioned under the logo' ),
			'before_widget' => '<div id="%1$s" class="text %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="text-title">',
			'after_title' => '</h4>',
		) );
		

	register_sidebar( array(
			'id' => 'about',
			'name' => __( 'about' ),
			'description' => __( 'About' ),
			'before_widget' => '<div id="%1$s" class="about homepage-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="about-title">',
			'after_title' => '</h4>',
		) );


		// Sidebar second row production
	register_sidebar( array(
			'id' => 'production',
			'name' => __( 'production' ),
			'description' => __( 'Production' ),
			'before_widget' => '<div id="%1$s" class="production homepage-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="production-title">',
			'after_title' => '</h4>',
		) );
		
				// Sidebar second row testimonials
	register_sidebar( array(
			'id' => 'testimonials',
			'name' => __( 'testimonials' ),
			'description' => __( 'This widget is positioned in the third row of the homepage' ),
			'before_widget' => '<div id="%1$s" class="testimonials homepage-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="testimonials-title">',
			'after_title' => '</h4>',
		) );
		
		
			// Sidebar third row qparts
	register_sidebar( array(
			'id' => 'qparts',
			'name' => __( 'qparts' ),
			'description' => __( 'QParts' ),
			'before_widget' => '<div id="%1$s" class="qparts homepage-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="qparts-title">',
			'after_title' => '</h4>',
		) );
		
				// Sidebar third row sale
	register_sidebar( array(
			'id' => 'sale',
			'name' => __( 'sale' ),
			'description' => __( 'Sales' ),
			'before_widget' => '<div id="%1$s" class="sale homepage-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="sale-title">',
			'after_title' => '</h4>',
		) );
		
						// Sidebar bottom row news
	register_sidebar( array(
			'id' => 'news',
			'name' => __( 'news' ),
			'description' => __( 'News' ),
			'before_widget' => '<div id="%1$s" class="news homepage-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="news-title">',
			'after_title' => '</h4>',
		) );
		
						// Sidebar bottom  row facebook
	register_sidebar( array(
			'id' => 'facebook',
			'name' => __( 'facebook' ),
			'description' => __( 'This widget is positioned in the third row of the homepage' ),
			'before_widget' => '<div id="%1$s" class="facebook %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="facebook-title">',
			'after_title' => '</h4>',
		) );
		
						// Sidebar bottom row twitter
	register_sidebar( array(
			'id' => 'twitter',
			'name' => __( 'twitter' ),
			'description' => __( 'This widget is positioned in the third row of the homepage' ),
			'before_widget' => '<div id="%1$s" class="twitter %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="twitter-title">',
			'after_title' => '</h4>',
		) );
		
		

//Adds the stuff for the front slider
add_action( 'abbey_front_slider_loop_item', 'abbey_add_yith', 1 );

function abbey_add_yith() {
	global $product;
	
	$link = get_permalink($product->ID);
	
	echo '<h4 class="product-title"><a href="'.$link.'"></a><a href="'.$link.'">'.get_the_title($product->ID).'</a></h4>';
	echo '<div class="product_thumbnail" >'.get_the_post_thumbnail($product->ID,'medium').'</div>';
	echo '<div class="product_excerpt" >'.get_the_excerpt($product->ID).'</div>';
}



function abbey_scripts() {
//	wp_enqueue_style( 'theme-style', get_stylesheet_uri(), array( 'reset' ) );
	wp_enqueue_script( 'sticky', get_stylesheet_directory_uri() . '/js/sticky/jquery.sticky.js', array( 'jquery' ) );
	wp_enqueue_script( 'abbey', get_stylesheet_directory_uri() . '/js/abbey.js', array( 'jquery' ) );
}

add_action( 'wp_enqueue_scripts', 'abbey_scripts' );

/**
* separate media categories from post categories
* use a custom category called 'category_media' for the categories in the media library
*/
add_filter( 'wpmediacategory_taxonomy', function(){ return 'category_media'; } ); //requires PHP 5.3 or newer


add_action('admin_footer','my_tinyMCE');

function my_tinyMCE(){

        echo "
        <script>
                jQuery(document).ready(function() {
                        var my_textarea_id ='accordion_textbox'; // change this to match your textarea id
                        if(jQuery('#'+my_textarea_id).length){
                                tinyMCE.execCommand('mceAddControl', false, my_textarea_id);
                        }
                });
        </script>";

}

//woocommerce_subcategory_thumbnail - hide
function woocommerce_subcategory_thumbnail( $category ) 
{
	return;
}

//custom subcategories
function abbey_display_subcategories($category)
{
	if(is_shop())
	{
		//echo "Subcategory here";
		wc_get_template( 'content-product_subcat.php', array(
					'category' => $category
		) );
	}
}

add_action( 'woocommerce_after_subcategory', 'abbey_display_subcategories' );

//Go straight to single product if there is only one in a category
/* Redirect if there is only one product in the category */
add_action( 'woocommerce_before_shop_loop', 'abbey_woocommerce_redirect_if_single_product_in_category', 10 );
function abbey_woocommerce_redirect_if_single_product_in_category ($wp_query) {
	global $wp_query;
	if (is_product_category()) {
		if ($wp_query->post_count==1) {
			$product = new WC_Product($wp_query->post->ID);
			if ($product->is_visible()) wp_safe_redirect( get_permalink($product->id), 302 );
			exit;
		}
	}
}

function abbey_nopaging_downloads($query) {
	
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	
	
	
	//print_r($term);
	
	
	if ( $term->name = "Q-Parts") {
		$query->set('nopaging', 1);
	}
}

add_action('parse_query', 'abbey_nopaging_downloads');


remove_action('init',  'activate_account_via_email_link', 1);
add_action('init',  'abbey_activate_account_via_email_link', 1);

function abbey_activate_account_via_email_link()
{
	global $ultimatemember;

	if ( isset($_REQUEST['act']) && $_REQUEST['act'] == 'activate_via_email' && isset($_REQUEST['hash']) && is_string($_REQUEST['hash']) && strlen($_REQUEST['hash']) == 40 &&
	isset($_REQUEST['user_id']) && is_numeric($_REQUEST['user_id']) ) { // valid token

		$user_id = absint( $_REQUEST['user_id'] );
		delete_option( "um_cache_userdata_{$user_id}" );

		um_fetch_user( $user_id );

		if (  strtolower($_REQUEST['hash']) !== strtolower( um_user('account_secret_hash') )  )
			wp_die( __( 'This activation link is expired or have already been used.','ultimatemember' ) );

	//	$ultimatemember->user->approve();
		$ultimatemember->user->set_status('awaiting_admin_review');

		$redirect = ( um_user('url_email_activate') ) ? um_user('url_email_activate') : um_get_core_page('login', 'account_active');
		$login    = (bool) um_user('login_email_activate');

		// log in automatically
		if ( !is_user_logged_in() && $login ) {
			$user = get_userdata($user_id);
			$user_id = $user->ID;

			// update wp user
			wp_set_current_user( $user_id, $user_login );
			wp_set_auth_cookie( $user_id );

			ob_start();
			do_action( 'wp_login', $user_login );
			ob_end_clean();
		}

		um_reset_user();

		do_action('um_after_email_confirmation', $user_id );

		exit( wp_redirect( $redirect ) );
	}
}

?>
